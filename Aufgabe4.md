# Aufgabe 4

## Schritt 1

Was ist der OUI-Teil der MAC-Adresse für dieses Gerät?

5C-26-0A

Wie lautet der Seriennummernteil der MAC-Adresse für dieses Gerät?

24-2A-60

Ermitteln Sie anhand des obigen Beispiels den Namen des Anbieters, der diese Netzwerkkarte hergestellt hat.

Dell Inc.

B. Geben Sie an der Eingabeaufforderung auf PC-A den Befehl ipconfig /all ein und identifizieren Sie den OUI-Teil der MAC-Adresse für die Netzwerkkarte von PC-A.

Die Antworten variieren je nach Hersteller.

Identifizieren Sie den Seriennummernteil der MAC-Adresse für die Netzwerkkarte von PC-A.

Die Antworten variieren je nach Seriennummerncode des Herstellers.

Identifizieren Sie den Namen des Anbieters, der die Netzwerkkarte von PC-A hergestellt hat.

Die Antworten variieren je nach OUI des Herstellers.

## Schritt 2

Wie lautet die MAC-Adresse für VLAN 1 auf S1?

Die Antworten variieren je nach dem Schalter, den der Schüler verwendet. Mit der Ausgabe von oben wäre die Antwort 001b.0c6d.8f40.

Wie lautet die MAC-Seriennummer für VLAN 1?

Die Antworten variieren je nach dem Schalter, den der Schüler verwendet. Mit der Ausgabe von oben wäre die Antwort 6d-8f-40.

Was ist die OUI für VLAN 1?

Die Antworten variieren je nach dem Schalter, den der Schüler verwendet. Mit der Ausgabe von oben wäre die Antwort 00-1b-0c.

Wie lautet auf Grundlage dieser OUI der Name des Anbieters?

Cisco-Systeme

Wofür steht Bia?

Adresse eingebrannt.

Warum wird in der Ausgabe zweimal dieselbe MAC-Adresse angezeigt?

Die MAC-Adresse kann über einen Softwarebefehl geändert werden. Die tatsächliche Adresse (bia) wird weiterhin vorhanden sein. Es wird in Klammern angezeigt.

Welche Layer-2-Adressen werden auf S1 angezeigt?

S1 VLAN 1 und PC-A MAC-Adressen. Wenn der Schüler auch die MAC-Adressen notiert, variieren seine Antworten.

Welche Layer-3-Adressen werden auf S1 angezeigt?

S1- und PC-A-IP-Adressen

## Reflection Questions

1. Sind Übertragungen auf Layer-2-Ebene möglich? Wenn ja, wie wäre die MAC-Adresse?

Sie können Broadcasts auf Layer 2 durchführen. ARP verwendet Broadcasts, um MAC-Adressinformationen zu finden. Die Broadcast-Adresse lautet FF.FF.FF.FF.FF.FF.

2. Warum müssen Sie die MAC-Adresse eines Geräts kennen?

Dafür kann es verschiedene Gründe geben. In einem großen Netzwerk kann es einfacher sein, den Standort und die Identität eines Geräts anhand seiner MAC-Adresse statt anhand seiner IP-Adresse zu bestimmen. Die MAC-OUI listet den Hersteller auf, was bei der Eingrenzung der Suche hilfreich sein kann. Auf Layer 2 können Sicherheitsmaßnahmen angewendet werden, daher ist die Kenntnis der zulässigen MAC-Adressen erforderlich.