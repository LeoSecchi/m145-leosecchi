# Aufgabe 5

## Part 1

## Step 1
Ist diese Adresse in der obigen Tabelle aufgeführt?
Nein

Klicken Sie auf "Erfassen/Weiterleiten", um die PDU an das nächste Gerät zu übertragen.

Wie viele Kopien der PDU hat Switch1 erstellt?
3

Wie lautet die IP-Adresse des Geräts, das die PDU angenommen hat?
172.16.31.3

Öffnen Sie die PDU und untersuchen Sie die Schicht 2.

Was ist mit den MAC-Adressen von Quelle und Ziel passiert?
Die Quelle wurde zum Ziel, FFFF.FFFF.FFFF wurde zur MAC-Adresse von 172.16.31.3

Klicken Sie auf Erfassen/Weiterleiten, bis die PDU an 172.16.31.2 zurückkehrt.

Wie viele Kopien der PDU hat der Switch während der ARP-Antwort erstellt?
1

# Step 2

Beachten Sie, dass das ICMP-Paket wieder auftaucht. Öffnen Sie die PDU und untersuchen Sie die MAC-Adressen.

Stimmen die MAC-Adressen von Quelle und Ziel mit ihren IP-Adressen überein?
Ja

Wechseln Sie zurück zu Realtime und schließen Sie den Ping ab.

Klicken Sie auf 172.16.31.2 und geben Sie den Befehl arp-acommand ein.

Welcher IP-Adresse entspricht der Eintrag der MAC-Adresse?
172.16.31.3

Wann stellt ein Endgerät im Allgemeinen eine ARP-Anfrage?
Wenn es die MAC-Adresse des Empfängers nicht kennt.

## Part 2


## Step 1

Wie viele Antworten wurden gesendet und empfangen?
4 gesendet, 4 empfangen.


## Step 2


Stimmen die Angaben mit denen in der obigen Tabelle überein?
Ja

Stimmen die Einträge mit denen in der obigen Tabelle überein?
Ja

Warum sind zwei MAC-Adressen mit einem Port verbunden?
Weil beide Geräte über den Access Point mit einem Port verbunden sind.


## Part 3

## Step 1

Wie lautet die IP-Adresse des neuen ARP-Tabelleneintrags?
172.16.31.1

Wie viele PDUs erscheinen?
2

Wie lautet die Ziel-IP-Zieladresse der ARP-Anfrage?
172.16.31.1

Die Ziel-IP-Adresse ist nicht 10.10.10.1.

Warum?
Die Gateway-Adresse der Router-Schnittstelle ist in der IPv4-Konfiguration der Hosts gespeichert. Befindet sich der empfangende Host nicht im selben Netz, verwendet die Quelle den ARP-Prozess, um eine MAC-Adresse für die als Gateway dienende Router-Schnittstelle zu ermitteln.

## Step 2

Wie viele MAC-Adressen befinden sich in der Tabelle? Warum?
Null. Dieser Befehl bedeutet etwas ganz anderes als der Switch-Befehl show mac address-table.

Gibt es einen Eintrag für 172.16.31.2?
Ja

Was passiert mit dem ersten Ping in einer Situation, in der der Router auf die ARP-Anfrage antwortet?
Es kommt zu einem Timeout.


