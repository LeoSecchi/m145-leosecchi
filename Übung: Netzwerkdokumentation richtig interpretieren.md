# Übung: Netzwerkdokumentation richtig interpretieren

Wie lautet die Netzwerkadresse vom Standort Samedan? 192.168.3.1

Auf welcher IP-Adresse befindet sich der AccessPoint in Bellinzona? 192.168.4.30

In welchem VLAN befinden sich die Arbeitsplatz-PCs? VLAN-2: Office (Port 1-23)

Über welche IP-Adresse erreicht man den Manageable-Switch am Standort Chur? Router/Firewall//Veb-Proxy IPFire 2.17/C90 (NAT) GW: 192.168.4.1 DHCP: 192.168.4.1 DNS: 62.2.32.158 VPN-B: 172.200.4.2/24 CATV-Modem (dyn. IP) ups cablecom «Internet 125» DynDNS: belli.ingcaprez.ch

Welche IP-Adressen LAN-seitig und tunnelseitig ist der VPN-Gateway am Standort Bellinzona konfiguriert? 
Die LAN-seitige IP-Adresse des VPN-Gateways am Standort Bellinzona lautet 192.168.4.1. Die tunnelseitige IP-Adresse lautet 172.200.4.2/24.

QUELLEN

Seite 4: LAN-seitige IP-Adresse des VPN-Gateways am Standort Bellinzona
Seite 4: Tunnelseitige IP-Adresse des VPN-Gateways am Standort Bellinzona


Wie lautet der am Standort Samedan an den Arbeitsplatz-PCs eingetragene Standardgateway? Bei den Arbeitsplatz-PCs in Samedan ist als Standard-Gateway 192.168.4.1 eingetragen.


Ein Aussendienstmitarbeiter benötigt Zugriff auf das Firmennetzwerk. Wie muss er seine VPN-SW IP-mässig konfigurieren?
Die VPN-Software des Außendienstmitarbeiters muss mit den folgenden IP-Einstellungen konfiguriert werden, um auf das Firmennetz zugreifen zu können:

VPN-A (PtP) mit der IP-Adresse 172.200.3.2/24
VPN-B (PtP) mit der IP-Adresse 172.200.4.1/24
VPN-C (RAS) für den Fernzugriff

Wer hat im Büro CAD Wasserbau die VoIP-Telefone installiert? Die VoIP-Telefone im Büro CAD Wasserbau wurden von "abisang" installiert.

Wann wurde im Bistro der AccesPoint installiert? 23.7.14

Der IT-Mitarbeiter Rene Sauter (rsauter) verlässt die Firma. Welche Arbeiten bzw. Verantwortlichkeiten sind davon betroffen? Wenn würden sie als zukünftigen Ansprechpartner bei Problemen vorschlagen? IT employee Rene Sauter

Wieviele RJ45-Steckdosen stehen ihnen im Bauleiterbüro zur Verfügung und wieviele davon sind zurzeit noch nicht belegt? Im Bauleiterbüro stehen insgesamt 6 RJ45-Steckdosen zur Verfügung. Zum aktuellen Zeitpunkt sind 2 davon noch nicht belegt.

Based on the information provided, to set up an additional VoIP-Telefon in the CAD Tiefbau office, the connection should be patched as follows:

Patchpanel Belegung:

Anschluss: E2/3
Lokation: CAD Tiefbau
Status: Belegt
Beschreibung: VoIP-Telefon
Switch-Port:

Port am Switch: VLAN-2: Office (Port 1-23)

Im Bistro soll eine Projektpräsentation stattfinden. Dafür muss ein Ethernetkabelverbindung bereitgestellt werden.
Gemäss den Angaben ist das Bistro am Hauptsitz des Unternehmens in Chur mit einem Ethernet-Anschluss ausgestattet. Aus dem Netzwerkplan geht hervor, dass das Bistro über einen Access Point verfügt, und aus dem Verkabelungsplan und der Patchpanel-Dokumentation sollte hervorgehen, wo genau sich der Ethernet-Anschluss befindet. Falls der Ethernet-Anschluss im Bistro noch nicht benutzt wird, sollte er für die Einrichtung der für die Projektpräsentation benötigten Ethernet-Kabelverbindung zur Verfügung stehen.

Im Büro CAD Wasserbau soll temporär ein weiterer Arbeitsplatz eingereichtet werden. Wie werden sie diese Aufgabe lösen?
1. Überprüfen Sie die Verfügbarkeit von Netzwerkanschlüssen: Schauen Sie auf dem Patchpanel und in der Belegungsliste nach, welche Netzwerkanschlüsse für den neuen Arbeitsplatz frei sind.

2. Schließen Sie die erforderlichen Geräte an: Sobald ein freier Netzwerkanschluss gefunden ist, schließen Sie den neuen Arbeitsplatz mit der entsprechenden Verkabelung und Netzwerkausrüstung an das Netzwerk an.

3. Konfigurieren Sie die Workstation: Richten Sie die neue Arbeitsstation mit der erforderlichen Software und den erforderlichen Zugriffsrechten ein und stellen Sie sicher, dass sie den Netzwerk- und Sicherheitsrichtlinien entspricht.

4. Dokumentieren Sie die Änderung: Aktualisieren Sie die Belegungsliste und die Netzwerkdokumentation, um die neue Verbindung zu berücksichtigen, und stellen Sie sicher, dass alle Änderungen für künftige Referenzen ordnungsgemäß dokumentiert werden.

5.Testen Sie die Verbindung: Überprüfen Sie, ob der neue Arbeitsplatz eine Verbindung zum Netzwerk herstellen und auf die erforderlichen Ressourcen zugreifen kann.

Wieviele Switchs stehen ihnen am Standort Chur zur Verfügung? 2


Frau Sommer arbeitet an der CAD-Workstation und meldet ihnen, dass sie keine Verbindung ins Internet mehr hat. Was werden sie überprüfen? 1. Überprüfen Sie die Netzwerkkabelverbindung zur CAD-Arbeitsstation, um sicherzustellen, dass sie richtig angeschlossen ist.
2. Überprüfen Sie den Netzwerkanschluss, um sicherzustellen, dass er aktiv und richtig auf dem Patchpanel gepatcht ist.
3. Überprüfen Sie den Netzwerk-Switch, an den die CAD-Workstation angeschlossen ist, um sicherzustellen, dass er ordnungsgemäß funktioniert.
4. Überprüfen Sie die Netzwerkkonfiguration der CAD-Workstation, um sicherzustellen, dass sie die richtigen IP-Einstellungen und DNS-Konfiguration hat.

Wie lautet die SSID des Churer-AccessPoint?
Die SSID des Churer-AccessPoints ist "ZyXEL NWA1123-AC."

