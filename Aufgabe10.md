# Auftrag 10 Fragen

## Part 1

## Step 1

Waren die Pings erfolgreich? Und warum?
Nein, die Pings waren nicht erfolgreich, weil sich PC1 in einem anderen VLAN befindet als PC6, so dass diese Geräte nicht miteinander kommunizieren können, weil sie logisch getrennt sind.

Schauen Sie sich das Simulationspanel an. Wohin hat S3 das Paket gesendet, nachdem es empfangen wurde?
S3 hat es an PC4 gesendet, da dieser sich im selben VLAN wie PC1 befindet.

## Step 2

Waren die Pings erfolgreich? Warum?
Ja, weil PC1 und PC4 beide zu VLAN 10 gehören, so dass der Pfad der ARP-Anfrage derselbe ist wie zuvor. Da PC4 das Ziel ist, antwortet er auf die ARP-Anfrage. PC1 kann dann den Ping mit der Ziel-MAC-Adresse von PC4 senden.

Examine the Simulation Panel. When the packet reached S1, why does it also forward the packet to PC7?
Because PC7 also belong to VLAN 10 and the ARP requests was for VLAN10, switches will forward to any devices that are connected to VLAN10 in their port

## part 3

1. Wenn ein PC in VLAN 10 eine Broadcast-Nachricht sendet, welche Geräte empfangen sie?
Alle Geräte, die sich in VLAN 10 befinden

2. Wenn ein PC in VLAN 20 eine Broadcast-Nachricht sendet, welche Geräte empfangen sie?
Alle Geräte, die sich im VLAN 20 befinden

3. Wenn ein PC in VLAN 30 eine Broadcast-Nachricht sendet, welche Geräte empfangen sie?
Alle Geräte, die sich im VLAN 30 befinden.

4. Was passiert mit einem Frame, der von einem PC in VLAN 10 an einen PC in VLAN 30 gesendet wird?
Er wird verworfen.

5. Welches sind die Kollisionsdomänen des Switches in Bezug auf die Ports?
Jeder Port ist eine eigene Kollisionsdomäne.

6. Wie groß sind die Broadcast-Domänen des Switches in Bezug auf die Ports?
Sie werden durch die Anzahl der VLANs des Switches geteilt.