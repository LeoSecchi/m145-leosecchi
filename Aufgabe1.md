# Aufgabe 1

## Cisco Packet Tracer
## Aufgabe 1
### Teil 1
Schritt 2.b
Wie lautet die Einstellung für Bits pro Sekunde?
Lösung: 9600

Schritt 2.c
Wie lautet die Eingabeaufforderung auf dem Bildschirm?
Lösung:

Schritt 3.a
Welcher Befehl beginnt mit dem Buchstaben 'C'?
Lösung: connect

Schritt 3.b
Welche Befehle werden angezeigt?
Lösung: telnet, terminal, traceroute
Welche Befehle werden angezeigt?
Lösung: telnet, terminal

### Teil 2
Schritt 1.a
Welche Informationen werden für den Befehl enable angezeigt?
Lösung: Privilegierte Befehle einschalten

Schritt 1.b
Was wird nach dem Drücken der Tabulatortaste angezeigt?
Lösung: enable
Was würde passieren, wenn Sie an der Eingabeaufforderung te eingeben?
Lösung: terminal

Schritt 1.c
Wie ändert sich die Eingabeaufforderung?
Lösung: # im Konfigurationsmodus

Schritt 1.d
Wenn Sie dazu aufgefordert werden, geben Sie das Fragezeichen (?) ein.
Lösung: # Alle verfügbaren Befehle
Wie viele Befehle werden jetzt angezeigt, da der privilegierte EXEC-Modus aktiv ist? (Tipp: Sie können c? eingeben, um nur die Befehle aufzulisten, die mit 'C' beginnen: # clear, clock, configure, connect, copy

Schritt 2.a
Wie lautet die angezeigte Meldung?Lösung: # Configure Terminal, Geben Sie die Konfigurationsbefehle ein, einen pro Zeile. Beenden Sie mit CNTL/Z

Schritt 2.b
Drücken Sie die Eingabetaste, um den in Klammern gesetzten Standardparameter zu akzeptieren. Wie ändert sich die Eingabeaufforderung?Lösung: #

### Teil 3 - Einstellen der Uhr
Schritt 1.a
S1# show clock Welche Informationen werden angezeigt? Welches Jahr wird angezeigt?Lösung: # 1. März 1999

Schritt 1.b
S1# clock<ENTER>Welche Informationen werden angezeigt?Lösung: # % Unvollständiger Befehl

Schritt 1.c
S1# clock<ENTER>Welche Informationen werden angezeigt?Lösung: # set Einstellen von Uhrzeit und Datum

Schritt 1.d
S1# Uhr einstellen ? Welche Informationen werden abgefragt?
Lösung: # hh:mm:ss Aktuelle Zeit